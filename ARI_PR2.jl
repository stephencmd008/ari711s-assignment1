### A Pluto.jl notebook ###
# v0.18.1

using Markdown
using InteractiveUtils

# ╔═╡ 897ec334-de84-11eb-07ed-f9a7e398be73
using Pkg; Pkg.add(["Flux","MLDatasets","CUDA","BSON","ProgressMeter","TensorBoardLogger"]); GC.gc()

# ╔═╡ 11355bab-2a41-479b-9eb7-1404cb691eec
 Pkg.add(["FileIO","Images","ImageMagick","PlutoUI"]); GC.gc()

# ╔═╡ 7acd75a3-7a41-4a1a-88cb-94cac12f6dc4
begin
	
using Flux
using Flux.Data: DataLoader
using Flux.Optimise: Optimiser, WeightDecay
using Flux: onehotbatch, onecold
using Flux.Losses: logitcrossentropy
using Statistics, Random
using Logging: with_logger
using TensorBoardLogger: TBLogger, tb_overwrite, set_step!, set_step_increment!
using ProgressMeter: @showprogress
import MLDatasets
import BSON

using Images, FileIO
	using PlutoUI

GC.gc()
	
end

# ╔═╡ 0c14a5c4-ece0-43db-96aa-8b9021432a2b
md"### Required packages"

# ╔═╡ 43d5999c-349f-4289-a943-9309b024b94e
md"### Precompile the Packages"

# ╔═╡ 58408b27-270c-4675-bb5c-e160c173eafd
md"### Get file paths for Images"

# ╔═╡ d5e16e8f-5d2e-4af3-82d5-83a699d69713
begin
	abs = pwd()

    normal_train = abs*"/chest_xray/train/NORMAL/".*readdir(abs*"/chest_xray/train/NORMAL")[1:30]
	pneumonia_train = abs*"/chest_xray/train/PNEUMONIA/".*readdir(abs*"/chest_xray/train/PNEUMONIA")[1:30]

    normal_test = abs*"/chest_xray/test/NORMAL/".*readdir(abs*"/chest_xray/test/NORMAL")[1:30]
	pneumonia_test = abs*"/chest_xray/test/PNEUMONIA/".*readdir(abs*"/chest_xray/test/PNEUMONIA")[1:30]
	
end

# ╔═╡ c65e9ef6-b8fc-4699-832f-5fffd7fa1624
md"### Load the Data"

# ╔═╡ 7250e599-a326-423f-9b66-4bc998ee6781
md"### Utility functions"

# ╔═╡ a905048f-644e-4419-9504-38c511c91519
begin
	
	
num_params(model) = sum(length, Flux.params(model)) 
round4(x) = round(x, digits=4)



function labels(v,str,rturn=str)
    temp = []
    for (indx,i) in enumerate(v)
        if occursin(str,v[indx])
            push!(temp,rturn)
        end
    end
    return temp
end


function process_image(path)
    img = load(path)
    img = Gray.(img)
    img = imresize(img,(227,227))
    img = Flux.unsqueeze(Float32.(img), 3)
    
    return img
end

	
end

# ╔═╡ f1fcd0a8-1a1d-4b0c-bbe4-36b5428b4d9b
function get_data()


    x_train = [normal_train;pneumonia_train]
    x_test = [normal_test;pneumonia_test]


p_train_labels = labels(x_train,"PNEUMONIA","pneumonia")
normal_train_labels=labels(x_train,"NORMAL","normal")

p_test_labels=labels(x_test,"PNEUMONIA","pneumonia")
normal_test_labels=labels(x_test,"NORMAL","normal")


y_train=vcat(normal_train_labels,p_train_labels)

y_test=vcat(normal_test_labels,p_test_labels)

    

x_train =  [process_image.(x) for x in  x_train]


x_test =  [process_image.(x) for x in  x_test]

    
Dict(:x_train => x_train,:x_test =>x_test ,:y_train => y_train ,:y_test =>y_test)
end

# ╔═╡ 6d5290c3-0987-4db8-98a1-4407437d6043
md"### Data preparations"

# ╔═╡ dea7555b-9ba8-4cea-bdaa-6646dfb068ea
function process_data()
    
#combine xtrain images into one array
x_train = get_data()[:x_train]
v= x_train[1] 
for i in 2:length(x_train) 
   v = hcat(v,x_train[i])
end

x_train = reshape(v,227,227,3,:)

#do the same for xtest
x_test = get_data()[:x_test]
v2= xtest[1]
for i in 2:length(x_test)
   v2 = hcat(v2,x_test[i])
end

x_test = reshape(v2,227,227,3,:)



y_train = get_data()[:y_train]
y_test = get_data()[:y_test]
    
y_train, y_test = onehotbatch(y_train, ["normal","pneumonia"]), onehotbatch(ytest, ["normal","pneumonia"])


end

# ╔═╡ fe91a2aa-f907-46c2-9125-425cd114321b
md"### A mosaic view of 10 normal train images"

# ╔═╡ 4ae2ca54-69c3-4734-a7b7-a3899dda3caa

mosaicview([imresize(load(x),227,227) for x in  [normal_train;pneumonia_train]][1:10]; fillvalue=0.5, npad=5, ncol=5, rowmajor=true)


# ╔═╡ 30b714b3-6fa5-4c2b-b156-44a2c873310f
md"### 10 pneumonia (test) images"

# ╔═╡ 45f15e3f-b6f0-4bcc-a605-7650ff5f0082

mosaicview([imresize(load(x),227,227) for x in  pneumonia_test][1:10]; fillvalue=0.5, npad=5, ncol=5, rowmajor=true)


# ╔═╡ 30b5369a-02bb-496a-9e81-39f1bfcb9d30
md"### AlexNet Architecture Implementation"

# ╔═╡ 0267e7aa-2c1d-4e0d-b1ca-6b73985e876a

function build_model()
	return Chain(
		
		Conv((11, 11), 1=>96, stride=4, relu),
		MaxPool((3,3), stride=2),
		Conv((5, 5), 96=>256, pad=2, relu),
		MaxPool((3,3), stride=2),
	    Conv((3, 3), 256=>384, pad=1, relu),
		Conv((3, 3), 384=>384, pad=1, relu),
		Conv((3, 3), 384=>256, pad=1, relu),
		MaxPool((3,3), stride=2),
		
		Flux.flatten,
	
		Dense(6400, 4096, relu),
		Dropout(0.5),
		Dense(4096, 4096, relu),
		Dropout(0.5),
		Dense(4096, 2),
	
	
		softmax
	)
end


# ╔═╡ b9687ab7-9e9f-4ddf-900d-b530853dff6e
loss(ŷ, y) = logitcrossentropy(ŷ, y)

# ╔═╡ ac74d9b4-1c15-42a5-b17c-8421bb8c5c7d
function eval_loss_accuracy(loader, model)
    l = 0f0
    acc = 0
    ntot = 0
    for (x, y) in loader
        ŷ = model(x)
        l += loss(ŷ, y) * size(x)[end]        
        acc += sum(onecold(ŷ) .== onecold(y))
        ntot += size(x)[end]
    end
    return (loss = l/ntot |> round4, acc = acc/ntot*100 |> round4)
end

# ╔═╡ e6a1c174-b74a-45e4-b165-017b1d6ce2f4
md"### main trainer function"

# ╔═╡ 2d9ae2ed-eda6-4b06-b4ac-6a502232c434
#size(x_train), size(y_train)

# ╔═╡ 66dc95b6-ab14-48c3-a4f8-24c2e7692be4
calculate_accuracy(x, y, m) = mean(Flux.onecold(m(x), 0:1) .== Flux.onecold(y, 0:1))

# ╔═╡ 443f3f3c-964a-40fe-8849-d1247a931f9c

function train_model(x_train, y_train)
	
	model = build_model() 
	
	
	opt = Momentum(learning_rate)

	
	augment(x) = x .+ 0.1f0 * gpu(randn(eltype(x), size(x)))
	
	loss(x, y) = sum(Flux.crossentropy(model(augment(x)), y))


	loss_over_time = []
	accuracy_over_time = []
	function loss_callback(epoch)
		x, y = gpu(x_val), gpu(y_val)
		l = loss(x, y)
		push!(loss_over_time, l)
		accuracy = calculate_accuracy(x, y, model)
		push!(accuracy_over_time, accuracy)
		@info("Epoch: $epoch, Loss: $l, Accuracy: $(accuracy)%")
	end

	
	@info("Batching training data...")
	data_loader = Flux.Data.DataLoader((x_train, y_train), batchsize=batchsize, shuffle=true)
	@info("Finished batching training data")
	ps = params(model)
	for epoch in 1:n_epochs
		@info("Training epoch $epoch...")
		for d in data_loader
			gs = gradient(ps) do
				l = loss(gpu(d)...)
			end
			Flux.update!(opt, ps, gs)
			GC.gc()
		end
		loss_callback(epoch)
	end

	return model, loss, loss_over_time, accuracy_over_time
end

# ╔═╡ f4805bd5-87a5-48e5-a1a0-4f065d46b4a0
function run_train()
	return train_model(
		x_train,
		y_train,
	)
end

# ╔═╡ 0ab28f70-0285-4971-b1b9-6b21de6adff8
model, loss_fn, loss_over_time, accuracy_over_time = run_train()

# ╔═╡ 4c81ca54-f11e-40cd-a4f5-4d5b249154f6
plot([loss_over_time, accuracy_over_time],
	ylabel="Loss",
	xlabel = ["Loss" "Accuracy"],
	title="Performance Per Epoch")

# ╔═╡ Cell order:
# ╠═0c14a5c4-ece0-43db-96aa-8b9021432a2b
# ╠═897ec334-de84-11eb-07ed-f9a7e398be73
# ╠═11355bab-2a41-479b-9eb7-1404cb691eec
# ╠═43d5999c-349f-4289-a943-9309b024b94e
# ╠═7acd75a3-7a41-4a1a-88cb-94cac12f6dc4
# ╠═58408b27-270c-4675-bb5c-e160c173eafd
# ╠═d5e16e8f-5d2e-4af3-82d5-83a699d69713
# ╠═c65e9ef6-b8fc-4699-832f-5fffd7fa1624
# ╠═f1fcd0a8-1a1d-4b0c-bbe4-36b5428b4d9b
# ╠═7250e599-a326-423f-9b66-4bc998ee6781
# ╠═a905048f-644e-4419-9504-38c511c91519
# ╠═6d5290c3-0987-4db8-98a1-4407437d6043
# ╠═dea7555b-9ba8-4cea-bdaa-6646dfb068ea
# ╠═fe91a2aa-f907-46c2-9125-425cd114321b
# ╠═4ae2ca54-69c3-4734-a7b7-a3899dda3caa
# ╠═30b714b3-6fa5-4c2b-b156-44a2c873310f
# ╠═45f15e3f-b6f0-4bcc-a605-7650ff5f0082
# ╠═30b5369a-02bb-496a-9e81-39f1bfcb9d30
# ╠═0267e7aa-2c1d-4e0d-b1ca-6b73985e876a
# ╠═b9687ab7-9e9f-4ddf-900d-b530853dff6e
# ╠═ac74d9b4-1c15-42a5-b17c-8421bb8c5c7d
# ╠═e6a1c174-b74a-45e4-b165-017b1d6ce2f4
# ╠═443f3f3c-964a-40fe-8849-d1247a931f9c
# ╠═2d9ae2ed-eda6-4b06-b4ac-6a502232c434
# ╠═66dc95b6-ab14-48c3-a4f8-24c2e7692be4
# ╠═f4805bd5-87a5-48e5-a1a0-4f065d46b4a0
# ╠═0ab28f70-0285-4971-b1b9-6b21de6adff8
# ╠═4c81ca54-f11e-40cd-a4f5-4d5b249154f6
